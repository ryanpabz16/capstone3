import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import { RouterProvider, createBrowserRouter } from "react-router-dom";
import NotFound from "./pages/NotFound";
import Header from "./components/Header";
import storeData from "./data/store.json";
import Register from "./pages/Register";
import UpdateProfile from "./pages/UpdateProfile";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import "@sweetalert2/theme-material-ui/material-ui.css";
import Profile from "./pages/Profile";
import Products from "./pages/Products";
import AddGame from "./pages/AddGame";
import EditGame from "./pages/EditGame";

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    // errorElement: <NotFound />,
    children: [
      {
        path: "/home",
        element: <Header title={storeData.name} subtitle={storeData.slogan} />,
      },
      {
        path: "/register",
        element: <Register />,
      },
      {
        path: "/login",
        element: <Login />,
      },
      {
        path: "/logout",
        element: <Logout />,
      },
      {
        path: "/profile",
        element: <Profile />,
      },
      {
        path: "/profile/register",
        element: <UpdateProfile register />,
      },
      {
        path: "/profile/edit",
        element: <UpdateProfile />,
      },
      {
        path: "/products",
        element: <Products />,
      },
      {
        path: "/products/add",
        element: <AddGame />,
      },
      {
        path: "/products/:gameId/edit",
        element: <EditGame />,
      },
      {
        path: "/*",
        element: <NotFound />,
      },
    ],
  },
]);

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
