const useStyles = () => ({
  form: {
    borderRadius: 5,
    maxWidth: "md",
    padding: 5,
    "& > *": {
      borderRadius: 5,
      marginY: 4,
    },
  },
  profile: {
    borderRadius: 5,
    padding: 5,
    "& > *": {
      marginY: 4,
    },
  },
});

export default useStyles;
