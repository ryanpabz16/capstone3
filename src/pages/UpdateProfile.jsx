import { Box, Button, Paper, TextField } from "@mui/material";
import Header from "../components/Header";
import { useEffect, useState } from "react";
import { handleFetch } from "../App";
import { DatePicker } from "@mui/x-date-pickers";
import { useNavigate } from "react-router-dom";
import dayjs from "dayjs";
import useStyles from "../styles";
import Swal from "sweetalert2";

export default function UpdateProfile({ register }) {
  const styles = useStyles();
  const navigate = useNavigate();

  const [name, setName] = useState("");
  const [birthDate, setBirthDate] = useState("");
  const [mobileNo, setMobileNo] = useState("");
  const [address, setAddress] = useState("");

  useEffect(() => {
    if (!register) {
      handleFetch(`users/${localStorage.getItem("username")}`).then(
        (result) => {
          if ("data" in result) {
            setName(result.data.fullName);
            setMobileNo(result.data.mobileNo);
            setAddress(result.data.address);
            setBirthDate(dayjs(result.data.birthDate));
          }
        }
      );
    }
  }, [register]);

  function handleSubmit(event) {
    event.preventDefault();

    handleFetch(`users/${localStorage.getItem("username")}`, "PUT", {
      fullName: name,
      birthDate: birthDate,
      mobileNo: mobileNo,
      address: address,
    }).then((result) => {
      if (register) {
        Swal.fire({
          icon: "success",
          title: "User profile successfully created",
        });

        navigate("/");
      } else if ("error" in result) {
        Swal.fire({
          icon: "error",
          title: result.message,
        });
      } else {
        Swal.fire({
          icon: "success",
          title: result.message,
        });
        navigate("/profile");
      }
    });
  }

  return (
    <>
      <Header subtitle={`${register ? "Complete" : "Edit"} you profile`} />

      <Paper
        component="form"
        elevation={12}
        sx={styles.form}
        onSubmit={handleSubmit}
      >
        <Box>
          <TextField
            required
            fullWidth
            label="Full Name"
            type="text"
            {...(register ? {} : { value: name })}
            onChange={(event) => setName(event.target.value)}
          />
        </Box>
        <Box sx={{ display: "flex", gap: 5 }}>
          <DatePicker
            required
            fullWidth
            label="Birth Date"
            disableFuture
            slotProps={{ textField: { fullWidth: true } }}
            openTo="year"
            {...(register ? {} : { value: birthDate })}
            onChange={(value) => {
              setBirthDate(value.toDate().toDateString());
            }}
          />
          <TextField
            required
            fullWidth
            label="Mobile Number"
            type="telephone"
            {...(register ? {} : { value: mobileNo })}
            onChange={(event) => setMobileNo(event.target.value)}
          />
        </Box>
        <Box>
          <TextField
            required
            fullWidth
            label="Address"
            type="text"
            {...(register ? {} : { value: address })}
            onChange={(event) => setAddress(event.target.value)}
          />
        </Box>
        <Button type="submit">{register ? "Register" : "Edit"} Profile</Button>
      </Paper>
    </>
  );
}
