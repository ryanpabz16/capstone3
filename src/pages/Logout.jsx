import { useContext, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../data/UserContext";

export default function Logout() {
  const navigate = useNavigate();
  const { setUser } = useContext(UserContext);

  useEffect(() => {
    if (localStorage.getItem("username")) {
      localStorage.clear();
      setUser("", false);

      Swal.fire({
        icon: "success",
        title: "User has logged out",
      });
    }

    navigate("/");
  }, [navigate, setUser]);
}
