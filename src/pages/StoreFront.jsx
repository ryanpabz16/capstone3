import { Box, Paper } from "@mui/material";
import Header from "../components/Header";
import { useEffect, useState } from "react";
import { handleFetch } from "../App";

export default function StoreFront() {
  const [games, setGames] = useState([]);

  useEffect(() => {
    handleFetch("products/").then((result) => {
      if ("data" in result) {
        setGames(result.data);
      }
    });
  }, []);

  const gameCards = {};

  return (
    <Box>
      <Header title="GameWorld" />
    </Box>
  );
}
