import { useContext, useState } from "react";
import useStyles from "../styles";
import Header from "../components/Header";
import { Box, Button, Paper, TextField } from "@mui/material";
import { handleFetch } from "../App";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../data/UserContext";

export default function Login() {
  const styles = useStyles();
  const navigate = useNavigate();
  const { setUser } = useContext(UserContext);

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  function handleSubmit(event) {
    event.preventDefault();

    handleFetch("users/login", "POST", {
      username: username,
      password: password,
    }).then((result) => {
      if ("token" in result) {
        localStorage.setItem("token", result.token);
        localStorage.setItem("username", username);
        setUser(username, result.isAdmin);

        Swal.fire({
          icon: "success",
          title: result.message,
        });
        navigate("/");
      } else {
        Swal.fire({
          icon: "error",
          title: result.message,
        });
      }
    });
  }

  return (
    <>
      <Header title="Log in" subtitle="Sign in to your account" />
      <Paper
        component="form"
        elevation={12}
        sx={styles.form}
        onSubmit={handleSubmit}
      >
        <Box>
          <TextField
            required
            fullWidth
            label="Username"
            type="text"
            onChange={(event) => setUsername(event.target.value)}
          />
        </Box>
        <Box>
          <TextField
            required
            fullWidth
            label="Password"
            type="password"
            onChange={(event) => setPassword(event.target.value)}
          />
        </Box>
        <Button type="submit">Log in</Button>
      </Paper>
    </>
  );
}
