import {
  Box,
  Button,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import Header from "../components/Header";
import { useContext, useEffect, useState } from "react";
import UserContext from "../data/UserContext";
import { useNavigate } from "react-router-dom";
import { handleFetch } from "../App";

export default function Products() {
  const navigate = useNavigate();
  const { isAdmin } = useContext(UserContext);
  const [gameData, setGameData] = useState([]);

  useEffect(() => {
    if (!isAdmin) {
      navigate("/");
    } else {
      handleFetch("products").then((result) => {
        console.log(result);
        if ("data" in result) {
          setGameData(result.data);
        }
      });
    }
  }, [isAdmin, navigate]);

  return (
    <Box>
      <Header title="Game List" />
      <Button onClick={() => navigate("/products/add")}>Add Game</Button>
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Title</TableCell>
              <TableCell>Description</TableCell>
              <TableCell>Price</TableCell>
              <TableCell>Platforms</TableCell>
              <TableCell>Genre</TableCell>
              <TableCell>Metascore</TableCell>
              <TableCell>Release Date</TableCell>
              <TableCell>Publisher</TableCell>
              <TableCell>Developer</TableCell>
              <TableCell>Age Rating</TableCell>
              <TableCell>Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {gameData.map((game) => (
              <TableRow key={game._id}>
                <TableCell>{game.title}</TableCell>
                <TableCell>{game.description}</TableCell>
                <TableCell>{game.price}</TableCell>
                <TableCell>
                  {game.platforms.map((platform) => (
                    <Typography key={`${game}-${platform}`}>
                      {platform}
                    </Typography>
                  ))}
                </TableCell>
                <TableCell>
                  {game.genre.map((genre) => (
                    <Typography key={`${game}-${genre}`}>{genre}</Typography>
                  ))}
                </TableCell>
                <TableCell>{game.metascore}</TableCell>
                <TableCell>{game.releaseDate}</TableCell>
                <TableCell>{game.publisher}</TableCell>
                <TableCell>{game.developer}</TableCell>
                <TableCell>{game.ageRating}</TableCell>
                <TableCell>
                  <Button>{game.isActive ? "Archive" : "Activate"} Game</Button>
                  <Button onClick={() => navigate(`${game._id}/edit`)}>
                    Edit Info
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  );
}
