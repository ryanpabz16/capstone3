import { Box, Button, Paper, TextField } from "@mui/material";
import Header from "../components/Header";
import useStyles from "../styles";
import { useState } from "react";
import { handleFetch } from "../App";
import { useNavigate } from "react-router-dom";

export default function AddGame() {
  const styles = useStyles();
  const navigate = useNavigate();

  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [platforms, setPlatforms] = useState([]);
  const [genre, setGenre] = useState([]);
  const [metascore, setMetascore] = useState(0);
  const [releaseDate, setReleaseDate] = useState("");
  const [publisher, setPublisher] = useState("");
  const [developer, setDeveloper] = useState("");
  const [ageRating, setAgeRating] = useState("");

  function handleSubmit(event) {
    event.preventDefault();

    console.log({
      title: title,
      description: description,
      price: price,
      platforms: platforms,
      genre: genre,
      metascore: metascore,
      releaseDate: releaseDate,
      publisher: publisher,
      developer: developer,
      ageRating: ageRating,
    });

    console.log({
      title: typeof title,
      description: typeof description,
      price: typeof price,
      platforms: typeof platforms,
      genre: typeof genre,
      metascore: typeof metascore,
      releaseDate: typeof releaseDate,
      publisher: typeof publisher,
      developer: typeof developer,
      ageRating: typeof ageRating,
    });

    handleFetch("products/", "POST", {
      title: title,
      description: description,
      price: price,
      platforms: platforms,
      genre: genre,
      metascore: metascore,
      releaseDate: releaseDate,
      publisher: publisher,
      developer: developer,
      ageRating: ageRating,
    }).then((result) => {
      console.log(result);
    });

    navigate(-1);
  }

  return (
    <Box>
      <Header title="Add game" />
      <Paper
        component="form"
        elevation={12}
        sx={styles.form}
        onSubmit={handleSubmit}
      >
        <Box>
          <TextField
            required
            fullWidth
            label="Title"
            type="text"
            onChange={(event) => setTitle(event.target.value)}
          />
        </Box>
        <Box>
          <TextField
            required
            fullWidth
            label="Description"
            type="text"
            onChange={(event) => setDescription(event.target.value)}
          />
        </Box>
        <Box>
          <TextField
            required
            fullWidth
            step="any"
            label="Price"
            type="number"
            onChange={(event) => setPrice(Number(event.target.value))}
          />
        </Box>
        <Box>
          <TextField
            required
            fullWidth
            label="Platforms"
            type="text"
            onChange={(event) =>
              setPlatforms(
                event.target.value.split(",").map((key) => key.trim())
              )
            }
          />
        </Box>
        <Box>
          <TextField
            required
            fullWidth
            label="Genre"
            type="text"
            onChange={(event) =>
              setGenre(event.target.value.split(",").map((key) => key.trim()))
            }
          />
        </Box>
        <Box>
          <TextField
            required
            fullWidth
            label="Metascore"
            type="text"
            onChange={(event) => setMetascore(Number(event.target.value))}
          />
        </Box>
        <Box>
          <TextField
            required
            fullWidth
            label="Release Year"
            type="text"
            onChange={(event) => setReleaseDate(event.target.value)}
          />
        </Box>
        <Box>
          <TextField
            required
            fullWidth
            label="Publisher"
            type="text"
            onChange={(event) => setPublisher(event.target.value)}
          />
        </Box>
        <Box>
          <TextField
            required
            fullWidth
            label="Developer"
            type="text"
            onChange={(event) => setDeveloper(event.target.value)}
          />
        </Box>
        <Box>
          <TextField
            required
            fullWidth
            label="Age Rating"
            type="text"
            onChange={(event) => setAgeRating(event.target.value)}
          />
        </Box>
        <Button type="submit">Submit</Button>
      </Paper>
    </Box>
  );
}
