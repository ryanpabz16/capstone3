import { Box, Button, Paper, TextField } from "@mui/material";
import Header from "../components/Header";
import useStyles from "../styles";
import { useEffect, useState } from "react";
import { handleFetch } from "../App";
import { useNavigate, useParams } from "react-router-dom";

export default function AddGame() {
  const styles = useStyles();
  const navigate = useNavigate();
  const { gameId } = useParams();

  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [platforms, setPlatforms] = useState([]);
  const [genre, setGenre] = useState([]);
  const [metascore, setMetascore] = useState(0);
  const [releaseDate, setReleaseDate] = useState("");
  const [publisher, setPublisher] = useState("");
  const [developer, setDeveloper] = useState("");
  const [ageRating, setAgeRating] = useState("");

  useEffect(() => {
    handleFetch(`products/${gameId}`).then((result) => {
      if ("data" in result) {
        setTitle(result.data.title);
        setDescription(result.data.description);
        setPrice(result.data.price);
        setPlatforms(result.data.platforms);
        setGenre(result.data.genre);
        setMetascore(result.data.metascore);
        setReleaseDate(result.data.releaseDate);
        setPublisher(result.data.publisher);
        setDeveloper(result.data.developer);
        setAgeRating(result.data.ageRating);
      }
    });
  }, [gameId]);

  function handleSubmit(event) {
    event.preventDefault();

    handleFetch(`products/${gameId}`, "PUT", {
      title: title,
      description: description,
      price: price,
      platforms: platforms,
      genre: genre,
      metascore: metascore,
      releaseDate: releaseDate,
      publisher: publisher,
      developer: developer,
      ageRating: ageRating,
    }).then((result) => {
      console.log(result);
    });

    navigate(-1);
  }

  return (
    <Box>
      <Header title="Add game" />
      <Paper
        component="form"
        elevation={12}
        sx={styles.form}
        onSubmit={handleSubmit}
      >
        <Box>
          <TextField
            required
            fullWidth
            label="Title"
            type="text"
            value={title}
            onChange={(event) => setTitle(event.target.value)}
          />
        </Box>
        <Box>
          <TextField
            required
            fullWidth
            label="Description"
            type="text"
            value={description}
            onChange={(event) => setDescription(event.target.value)}
          />
        </Box>
        <Box>
          <TextField
            required
            fullWidth
            step="any"
            label="Price"
            type="number"
            value={price}
            onChange={(event) => setPrice(event.target.value)}
          />
        </Box>
        <Box>
          <TextField
            required
            fullWidth
            label="Platforms"
            type="text"
            value={platforms.join(", ")}
            onChange={(event) => {
              console.log(event.target.value);
              setPlatforms(
                event.target.value.split(",").map((platform) => platform.trim())
              );
            }}
          />
        </Box>
        <Box>
          <TextField
            required
            fullWidth
            label="Genre"
            type="text"
            value={genre.join(", ")}
            onChange={(event) =>
              setGenre(
                event.target.value.split(",").map((genre) => genre.trim())
              )
            }
          />
        </Box>
        <Box>
          <TextField
            required
            fullWidth
            label="Metascore"
            type="text"
            value={metascore}
            onChange={(event) => setMetascore(event.target.value)}
          />
        </Box>
        <Box>
          <TextField
            required
            fullWidth
            label="Release Year"
            type="text"
            value={releaseDate}
            onChange={(event) => setReleaseDate(event.target.value)}
          />
        </Box>
        <Box>
          <TextField
            required
            fullWidth
            label="Publisher"
            type="text"
            value={publisher}
            onChange={(event) => setPublisher(event.target.value)}
          />
        </Box>
        <Box>
          <TextField
            required
            fullWidth
            label="Developer"
            type="text"
            value={developer}
            onChange={(event) => setDeveloper(event.target.value)}
          />
        </Box>
        <Box>
          <TextField
            required
            fullWidth
            label="Age Rating"
            type="text"
            value={ageRating}
            onChange={(event) => setAgeRating(event.target.value)}
          />
        </Box>
        <Button type="submit">Submit</Button>
      </Paper>
    </Box>
  );
}
