import { Typography } from "@mui/material";

export default function NotFound() {
  return (
    <Typography padding={10} variant="h1">
      404 | Not Found
    </Typography>
  );
}
