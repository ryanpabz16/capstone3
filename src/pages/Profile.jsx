import { Box, Button, Paper, Typography } from "@mui/material";
import { useContext, useEffect, useState } from "react";
import { handleFetch } from "../App";
import UserContext from "../data/UserContext";
import { useNavigate } from "react-router-dom";
import Header from "../components/Header";
import useStyles from "../styles";

export default function Profile() {
  const styles = useStyles();
  const navigate = useNavigate();
  const { username } = useContext(UserContext);
  const [userInfo, setUserInfo] = useState({});

  useEffect(() => {
    if (username) {
      handleFetch(`users/${username}`).then((result) => {
        setUserInfo(result.data);
      });
    } else {
      navigate("/");
    }
  }, [username, navigate]);

  return (
    <Box>
      <Header title="Profile" />
      <Paper sx={styles.profile} elevation={12}>
        <Typography>@{userInfo._id}</Typography>
        <Typography>{userInfo.fullName}</Typography>
        <Typography>Email: {userInfo.email}</Typography>
        <Typography>Mobile No: {userInfo.mobileNo}</Typography>
        <Typography>Birth Date: {userInfo.birthDate}</Typography>
        <Typography>Address: {userInfo.address}</Typography>
        <Button onClick={() => navigate("/profile/edit")}>Edit Profile</Button>
      </Paper>
    </Box>
  );
}
