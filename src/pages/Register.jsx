import { Alert, Box, Button, Paper, TextField } from "@mui/material";
import Header from "../components/Header";
import { useContext, useState } from "react";
import { handleFetch } from "../App";
import { useNavigate } from "react-router-dom";
import useStyles from "../styles";
import Swal from "sweetalert2";
import UserContext from "../data/UserContext";

export default function Register() {
  const styles = useStyles();
  const navigate = useNavigate();
  const { setUser } = useContext(UserContext);

  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [repassword, setRepassword] = useState("");

  const [passwordsMatch, setPasswordsMatch] = useState(true);

  async function handleSubmit(event) {
    event.preventDefault();

    console.log(password, repassword);
    if (password !== repassword) {
      setPasswordsMatch(false);
    } else {
      setPasswordsMatch(true);
    }

    if (passwordsMatch) {
      await handleFetch("users/register", "POST", {
        username: username,
        email: email,
        password: password,
      }).then((result) => {
        if ("error" in result) {
          Swal.fire({
            icon: "error",
            title: result.message,
          });

          return;
        }
      });

      handleFetch("users/login", "POST", {
        username: username,
        password: password,
      }).then((result) => {
        if ("token" in result) {
          localStorage.setItem("token", result.token);
          localStorage.setItem("username", username);
          setUser(username, result.isAdmin);

          navigate("/profile/register");
        } else {
          Swal.fire({
            icon: "error",
            title: result.message,
          });
        }
      });
    }
  }

  return (
    <>
      <Header title="Register" subtitle="Create a new user profile" />
      <Paper
        component="form"
        elevation={12}
        sx={styles.form}
        onSubmit={handleSubmit}
      >
        <Box>
          <TextField
            required
            fullWidth
            label="Username"
            type="text"
            onChange={(event) => setUsername(event.target.value)}
          />
        </Box>
        <Box>
          <TextField
            required
            fullWidth
            label="Email"
            type="email"
            onChange={(event) => setEmail(event.target.value)}
          />
        </Box>
        <Box>
          <TextField
            required
            fullWidth
            label="Password"
            type="password"
            onChange={(event) => setPassword(event.target.value)}
          />
        </Box>
        <Box>
          <TextField
            required
            fullWidth
            label="Confirm password"
            type="password"
            onChange={(event) => setRepassword(event.target.value)}
          />
        </Box>
        <Alert
          severity="error"
          sx={{ display: passwordsMatch ? "none" : "flex" }}
        >
          Passwords must match
        </Alert>
        <Button type="submit">Create Account</Button>
      </Paper>
    </>
  );
}
