import { Container, CssBaseline } from "@mui/material";
import React, { useEffect, useState } from "react";
import NavBar from "./components/Navbar";
import { Outlet } from "react-router-dom";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import UserContext from "./data/UserContext";

export default function App() {
  const [username, setUsername] = useState("");
  const [isAdmin, setIsAdmin] = useState(false);

  function setUser(username, isAdmin) {
    setUsername(username);
    setIsAdmin(isAdmin);
  }

  useEffect(() => {
    if (localStorage.getItem("username")) {
      handleFetch(`users/${localStorage.getItem("username")}`).then(
        (result) => {
          if ("data" in result) {
            setUser(result.data._id, result.data.isAdmin);
          }
        }
      );
    }
  }, []);

  return (
    <UserContext.Provider value={{ username, isAdmin, setUser }}>
      <LocalizationProvider dateAdapter={AdapterDayjs}>
        <CssBaseline />
        <NavBar />
        <main>
          <Container maxWidth="md">
            <Outlet />
          </Container>
        </main>
      </LocalizationProvider>
    </UserContext.Provider>
  );
}

export async function handleFetch(endpoint, method = "GET", body = null) {
  const url = `${process.env.REACT_APP_BUILD_API_URL}/${endpoint}`;
  const options = {
    method: method,
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  };

  if (!["GET", "DELETE"].includes(method)) {
    options.headers["Content-Type"] = "application/json";
    options.body = JSON.stringify(body);
  }

  return await fetch(url, options)
    .then((response) => response.json())
    .then((result) => result);
}
