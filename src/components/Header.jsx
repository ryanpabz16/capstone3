import { Container, Typography } from "@mui/material";

export default function Header({ title, subtitle }) {
  const styles = useStyles();

  return (
    <Container sx={styles.container} maxWidth="md">
      <Typography variant="h2" align="center" color="textPrimary" gutterBottom>
        {title}
      </Typography>
      <Typography variant="h4" align="center" color="textSecondary" paragraph>
        {subtitle}
      </Typography>
    </Container>
  );
}

const useStyles = () => ({
  container: {
    paddingY: 8,
  },
});
