import {
  Card,
  CardContent,
  CardMedia,
  Container,
  Grid,
  Typography,
} from "@mui/material";
import styles from "../styles";

export default function Album() {
  return (
    <Container sx={styles.cardGrid} maxWidth="md">
      <Grid container spacing={4}>
        <Grid item>
          <Card sx={styles.card}>
            <CardMedia
              sx={styles.cardMedia}
              image="https://source.unsplash.com/random"
            />
            <CardContent sx={styles.cardContent}>
              <Typography
                gutterBottom
                variant="h5"
                textAlign="center"
              ></Typography>
            </CardContent>
          </Card>
        </Grid>
        <Grid item>
          <Card sx={styles.card}>
            <CardMedia
              sx={styles.cardMedia}
              image="https://source.unsplash.com/random"
            />
            <CardContent sx={styles.cardContent}>
              <Typography
                gutterBottom
                variant="h5"
                textAlign="center"
              ></Typography>
            </CardContent>
          </Card>
        </Grid>
        <Grid item>
          <Card sx={styles.card}>
            <CardMedia
              sx={styles.cardMedia}
              image="https://source.unsplash.com/random"
            />
            <CardContent sx={styles.cardContent}>
              <Typography
                gutterBottom
                variant="h5"
                textAlign="center"
              ></Typography>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Container>
  );
}
