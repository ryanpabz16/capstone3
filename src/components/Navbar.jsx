import { MenuOutlined, PhotoCamera } from "@mui/icons-material";
import {
  AppBar,
  Box,
  Button,
  Divider,
  IconButton,
  Menu,
  MenuItem,
  Toolbar,
  Typography,
} from "@mui/material";
import { useContext, useEffect, useState } from "react";
import storeData from "../data/store";
import { NavLink } from "react-router-dom";
import UserContext from "../data/UserContext";

export default function NavBar() {
  const styles = useStyles();
  const { username, isAdmin } = useContext(UserContext);

  const [anchorEl, setAnchorEl] = useState(null);

  function handleOpenMenu(event) {
    setAnchorEl(event.target);
  }
  function handleCloseMenu(event) {
    setAnchorEl(null);
  }

  const defaultNavs = ["REGISTER", "LOGIN"];
  const userNavs = ["CART", "ORDERS", "PROFILE", "LOGOUT"];
  const adminNavs = ["PRODUCTS", "USERS", "ORDERS", "PROFILE", "LOGOUT"];

  function NavList({ variant, access }) {
    let navList = defaultNavs;
    if (access === "user") {
      navList = userNavs;
    } else if (access === "admin") {
      navList = adminNavs;
    }

    if (variant === "md") {
      return navList.map((item) => (
        <Button
          key={`${access}-${item}-md`}
          component={NavLink}
          to={`/${item.toLowerCase()}`}
          sx={styles.navButtons}
        >
          {item}
        </Button>
      ));
    } else if (variant === "xs") {
      return navList.map((item, index) => (
        <Box key={`${access}-${item}-xs`}>
          {index === 0 ? "" : <Divider variant="middle" />}
          <MenuItem
            component={NavLink}
            to={`/${item.toLowerCase()}`}
            onClick={handleCloseMenu}
          >
            <Typography size={14}>{item}</Typography>
          </MenuItem>
        </Box>
      ));
    }
  }

  const [access, setAccess] = useState("");

  useEffect(() => {
    if (isAdmin) {
      setAccess("admin");
    } else if (username) {
      setAccess("user");
    } else {
      setAccess("");
    }
  }, [username, isAdmin]);

  return (
    <AppBar sx={styles.appBar}>
      <Toolbar sx={styles.toolBar}>
        {/* Menu Navigation */}
        <Box>
          <IconButton
            size="large"
            aria-label="navigation"
            aria-controls="menu-appbar"
            aria-haspopup="true"
            onClick={handleOpenMenu}
            color="inherit"
          >
            <MenuOutlined sx={styles.menuIcon} />
          </IconButton>
          <Menu
            id="menu-appbar"
            anchorEl={anchorEl}
            anchorReference="anchorPosition"
            anchorPosition={{ top: 75, left: 0 }}
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "left",
            }}
            keepMounted
            transformOrigin={{
              vertical: "top",
              horizontal: "left",
            }}
            open={Boolean(anchorEl)}
            onClose={handleCloseMenu}
            sx={styles.menu}
          >
            <NavList variant="xs" access={access} />
          </Menu>
        </Box>
        {/* Titles */}
        <Box sx={styles.xsTitle}>
          <PhotoCamera />
          <Typography variant="h6">{storeData.name.toUpperCase()}</Typography>
        </Box>
        <Box sx={styles.mdTitle}>
          <PhotoCamera />
          <Typography variant="h6">{storeData.name.toUpperCase()}</Typography>
        </Box>
        {/* Navigation */}
        <Box sx={styles.navItems}>
          <NavList variant="md" access={access} />
        </Box>
      </Toolbar>
    </AppBar>
  );
}

const useStyles = () => ({
  appBar: {
    position: "relative",
  },
  toolBar: {
    position: "relative",
    display: "flex",
    justifyContent: "space-between",
  },
  xsTitle: {
    display: { xs: "flex", md: "none" },
    flexGrow: 1,
    justifyContent: "center",
    "& > *": {
      marginX: 1,
    },
  },
  mdTitle: {
    display: { xs: "none", md: "flex" },
    flexGrow: 1,
    "& > *": {
      marginRight: 2,
    },
  },
  navItems: {
    display: { xs: "none", md: "flex" },
    flexDirection: "row",
    "& > *": {
      marginRight: 2,
    },
  },
  navButtons: {
    color: "white",
    fontSize: 16,
  },
  menuIcon: {
    display: { xs: "flex", md: "none" },
  },
});
